# setup.py

from distutils.core import setup, Extension
import glob


cpp_files = glob.glob("**/*.cpp", recursive=True)
cxx_files = glob.glob("**/*.cxx", recursive=True)

module = Extension('_blossom', sources=cpp_files+cxx_files, include_dirs=['.'])

setup(name='blossom_v', ext_modules=[module], py_modules=["blossom_v"], author='Vladimir Kolmogorov')