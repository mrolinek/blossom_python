# Blossom

Perfect matching solver

## Installation

git clone git@gitlab.tuebingen.mpg.de:mvlastelica/blossom_python.git

cd blossom_python

swig -python -c++  PerfectMatching.i

python setup.py build_ext 

python setup.py install